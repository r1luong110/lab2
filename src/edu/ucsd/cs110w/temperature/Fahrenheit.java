/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110wce
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	
	public String toString()
	{
		// TODO: Complete this method
		return "" + this.getValue() + " F";
	}

	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return new Celsius((float)((this.getValue() - 32) / 1.8));
	}

	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return this;
	}
	
	public Temperature toKelvin()
	{
		return new Kelvin((float)(((this.getValue() - 32) / 1.8) + 273.15));
	}
}
